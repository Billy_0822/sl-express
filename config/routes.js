module.exports = {

  preMiddlewares: [
    '* requestLog requestParseURLEncoded requestParseBody',
    '/helloworld helloworld'
  ],

  routes: [
    'GET / PublicController.index',
    'GET /helloworld PublicController.helloworld',
    'GET /users PublicController.getUsers',
    'PUT /product PublicController.createProduct',
    'GET /products PublicController.getProducts',
    'DELETE /product PublicController.removeProduct',
    'POST /message PublicController.sendMessage'
  ],

  postMiddlewares: []
}
