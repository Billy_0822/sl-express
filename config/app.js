module.exports = {
  plugins: [
    'logger',
    'redis',
    'mongoose',
    'messageQueue',
    'helloworld',
    'queueTask'
  ]
};
