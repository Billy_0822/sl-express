const GoogleApiService = require('../services/GoogleApi');
const uuid = require('uuid');
const Queue = require('../models/Queue');
const SuccessResponse = {
  errcode: 0,
  errmsg: 'ok'
};
const ErrorResponse = {
  errcode: -1,
  errmsg: 'system error'
};

class PublicContoller {

  async index(req, res) {
    return res.send('Hello world');
  }

  async helloworld(req, res) {
    log('api', 'debug', {
      message: `the string from middleware is "${res.locals.string}"`
    });
    return res.send('helloworld');
  }

  // 查询用户列表
  async getUsers(req, res) {
    try {
      const users = await GoogleApiService.getUsers();
      return res.send({
        ...SuccessResponse,
        data: users
      });
    } catch (err) {
      log('api', 'error', {
        action: 'Controller.Public.getUsers',
        message: err.message
      });
      return res.send(ErrorResponse);
    }
  }

  // 创建商品
  async createProduct(req, res) {
    try {
      const { name, price } = req.body;
      // 创建商品
      await Product.create({
        name: name || ("product-" + uuid.v1()),
        price: price || Math.round(Math.random() * 100)
      });
      // 删除redis商品信息
      await app.redis.del('product');
      return res.send(SuccessResponse);
    } catch (err) {
      log('api', 'error', {
        action: 'Controller.Public.createProduct',
        message: err.message
      });
      return res.send(ErrorResponse);
    }
  }

  // 删除商品
  async removeProduct(req, res) {
    try {
      // 删除db商品
      const { productId } = req.body;
      await Product.remove({
        _id: productId
      });
      // 删除redis商品信息
      await app.redis.del('product');
      return res.send(SuccessResponse);
    } catch (err) {
      log('api', 'error', {
        action: 'Controller.Public.removeProduct',
        message: err.message
      });
      return res.send(ErrorResponse);
    }
  }

  // 查询商品列表
  async getProducts(req, res) {
    try {
      // const { offset, limit } = req.body;
      // 从redis取商品数据，若存在则直接返回
      const productInfo = await app.redis.get('product');
      log('api', 'debug', {
        action: 'product info from redis',
        message: productInfo
      });
      if (productInfo) {
        return res.send({
          ...SuccessResponse,
          data: JSON.parse(productInfo)
        });
      }
      // 从db查询商品信息并回写redis
      const list = await Product
        .find({}, '_id name price')
        .sort({ price: -1 });
      await app.redis.set('product', JSON.stringify(list));
      return res.send({
        ...SuccessResponse,
        data: list
      });
    } catch (err) {
      log('api', 'error', {
        action: 'Controller.Public.getProducts',
        message: err.message
      });
      return res.send(ErrorResponse);
    }
  }

  // 尝试发送消息到mq
  async sendMessage(req, res) {
    try {
      const { message } = req.body;
      await Queue.enqueue(message);
      return res.send(SuccessResponse);
    } catch (err) {
      log('api', 'error', {
        action: 'Controller.Public.sendMessage',
        message: err.message
      });
      return res.send(ErrorResponse);
    }
  }

}

module.exports = PublicContoller;
