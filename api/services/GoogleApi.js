const axios = require('axios');
const _ = require('lodash');

const client = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com',
  headers: { 'Content-Type': 'application/json' },
  timeout: 10000
});

class GoogleApiService {

  // 请求第三方获取用户列表
  static async getUsers() {
    try {
      // 请求 https://jsonplaceholder.typicode.com/todos 拿数据
      const { data: list } = await client.request({
        method: 'GET',
        url: '/todos'
      });
      // 根据id去重后提取用户列表
      const userIdSet = new Set();
      const users = [];
      for (const item of list) {
        const userId = item.userId;
        if (userIdSet.has(userId)) {
          continue;
        }
        users.push({
          userId
        });
        userIdSet.add(userId);
      }
      // 整合company字段到user
      for (const user of users) {
        Object.assign(user, {
          company: helpers.genCompany(user.userId)
        });
      }
      // 返回列表
      return users;
    } catch (err) {
      log('api', 'error', {
        action: 'Service.GoogleApi.getUsers',
        message: err.message
      });
      throw err;
    }
      
  }

}

module.exports = GoogleApiService;