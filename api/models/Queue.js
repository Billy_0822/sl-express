class Queue {
  // 消息入列
  static async enqueue(event) {
    log('message_queue', 'trace', {
      action: 'message enqueue',
      message: event
    });
    const payload = {
      event: event
    };
    await QueueTask.queue({
      taskType: 'TEST',
      payload: payload
    });
  }

  // 消息出列
  static async dequeue(queueTask) {
    const payload = queueTask.payload;
    log('message_queue', 'trace', {
      action: 'message dequeue',
      message: payload.event
    });
    // do something
  }
}

module.exports = Queue;