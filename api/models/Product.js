const mongoose = require('mongoose');

class ProductOrder extends MongooseModel {
  static schema() {
    return {
      // productId: { type: mongoose.ObjectId },
      name: { type: String, required: true },
      price: { type: Number, required: true }
    };
  }
}

module.exports = ProductOrder;