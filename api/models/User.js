const mongoose = require('mongoose');

class UserModel extends MongooseModel {
  static schema() {
    return {
      userId: { type: Number, required: true },
      company: { type: String, required: true }
    };
  }
}

module.exports = UserModel;