const uuid = require('uuid');
const UserCompanyMap = {};

module.exports = userId => {
    // 如果查不到用户对应公司则随机生成一个
    if (!UserCompanyMap[userId]) {
        UserCompanyMap[userId] = uuid.v4();
    }
    return UserCompanyMap[userId];
};