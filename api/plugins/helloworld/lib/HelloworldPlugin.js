class HelloworldPlugin {
  didLoadFramework() {
    // do something
    log('app', 'info', {
      message: 'HelloworldPlugin didLoadFramework success'
    });
  }

  prepare(app) {
    // do something
    log('app', 'info', {
      message: 'HelloworldPlugin prepare success'
    });
  }

  async connectDependencies(app) {
    // do something
    log('app', 'info', {
      message: 'HelloworldPlugin connectDependencies success'
    });
  }

  async disconnectDependencies(app) {
    // do something
    log('app', 'info', {
      message: 'HelloworldPlugin disconnectDependencies success'
    });
  }

  async willStartService(app) {
    // do something
    log('app', 'info', {
      message: 'HelloworldPlugin willStartService success'
    });
  }

  async didStartService(app) {
    // do something
    log('app', 'info', {
      message: 'HelloworldPlugin didStartService success'
    });
  }
}

module.exports = HelloworldPlugin;