const { App: SuperApp } = require('@shopline/sl-express')

class App extends SuperApp {
  async startService() {
    if (this.config.app.role == "CONSUMER") {
      // 启动mq消费服务
      await this.startConsumer();
      return;
    }
    // 启动普通服务
    await this.startExpress();
  }
}

let app = new App();
module.exports = app;
